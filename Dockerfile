FROM jboss/wildfly:9.0.2.Final

ADD standalone.xml /opt/jboss/wildfly/standalone/configuration/

ADD mysql-connector-java-5.1.42-bin.jar /opt/jboss/wildfly/modules/system/layers/base/com/mysql/main/

ADD module.xml /opt/jboss/wildfly/modules/system/layers/base/com/mysql/main/

RUN mkdir -p /opt/jboss/wildfly/standalone/configuration/PayrollConfig

COPY PayrollConfig /opt/jboss/wildfly/standalone/configuration/PayrollConfig

EXPOSE 8081

COPY PayrollWebEAR.ear /opt/jboss/wildfly/standalone/deployments/

RUN /opt/jboss/wildfly/bin/add-user.sh admin test@123 --silent
CMD /opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 -Dapi.host=$API_HOST:8081
